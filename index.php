<?php 
/**
 * Cứ mỗi người dùng khi làm việc với website đều được web server tạo ra một phiên làm việc riêng của người đó.
 * Session là một cách lưu trữ thông tin trên web server để có thể chia sẻ chúng cho tất cả các trang trong cùng ứng dụng.
 * Thông tin lưu trữ trong biến $_SESSION là tạm thời, nó sẽ bị hủy khi người truy cập không còn làm việc với website (đóng cửa sổ trình duyệt, truy cập một website khác, hoặc timeout).
 * Session được dùng để quản lý thông tin về mỗi người dùng người dùng riêng biệt.
 */

session_start();
$username = 'admin';
$password = '123456';
if (isset($_POST['submit'])) {
	if ($username == $_POST['username'] && $password == $_POST['password']) {
		$_SESSION['user'] = ['username' => $username, 'password' => $password];
		header('Location: avatar.php');
	} else {
		echo 'Dang nhap that bai';
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Dang nhap</title>
</head>
<style type="text/css">
	input[type=submit]{
	  margin-left: 0.5em;
	  height: 50px;
	  width: 250px;
	  padding: 0.2em 1em 0.2em 2.25em;
	  font-size: 25px;
	  font-weight: bold;
	  font-family: "Open Sans";
	  text-transform: uppercase;
	  color: #696666;
	  background: url(https://i.imgur.com/Th606mh.png) no-repeat scroll 0.75em 0.07em transparent;
	  background-size: 20px 100px;
	  border-radius: 2em;
	  border: 0.15em solid #F9C23C;
	  cursor: pointer;
	  transition: all 0.3s ease 0s;
}
input[type="submit"]:hover {
    color: #fff;
    background-color: #EAA502;
    border-color: #EAA502;
    background-position: 0.75em bottom;
    -webkit-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
input[type="submit"]:focus {
    background-position: 2em -4em;
    -webkit-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    transition: all 0.3s ease;
}

@font-face {
    font-family: 'Open Sans';
    font-style: normal;
    font-weight: 700;
    src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://themes.googleusercontent.com/static/fonts/opensans/v8/k3k702ZOKiLJc3WVjuplzHhCUOGz7vYGh680lGh-uXM.woff) format('woff');
}
</style>
<body background="./img/bg3.jpg" style="background-attachment:fixed;"> 

	<form action="" method="POST" accept-charset="utf-8" style="margin-top: 230px;">
		<table style="border: solid 1px; width: 500px;padding: 20px;margin: auto;" >
			<tr>
				<td colspan="2"><h1 style="text-align:center;">Đăng Nhập</h1></td>
			</tr>
			<tr>
				<td>Tài Khoản : </td>
				<td><input  type="text" name="username" value=""></td>
			</tr>
			<tr>
				<td>Mật Khẩu :</td>
				<td><input  type="password" name="password" value=""></td>
			</tr>
			<tr>
				<td colspan="2"><input style="margin-top: 20px;margin-left: 100px;" type="submit" name="submit" value="Đăng Nhập"></td>
			</tr>
		</table>
		
	</form>
</body>
</html>